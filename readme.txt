===============================================================================
              ACEGI SECURITY SYSTEM FOR SPRING - README FILE
===============================================================================

-------------------------------------------------------------------------------
OVERVIEW
-------------------------------------------------------------------------------

The Acegi Security System for Spring provides security services for 
The Spring Framework (http://www.springframework.org).

For a detailed list of features and access to the latest release, please visit
http://acegisecurity.org.

-------------------------------------------------------------------------------
SIGNED JAR FILES
-------------------------------------------------------------------------------

JAR files are no longer signed. They were signed in releases 1.0.0 and earlier.

-------------------------------------------------------------------------------
BUILDING
-------------------------------------------------------------------------------

Acegi Security is built using Maven. Please read the "Building with Maven" page
at http://acegisecurity.org. This page is also included in the /docs directory
of official release ZIPs.

-------------------------------------------------------------------------------
QUICK START
-------------------------------------------------------------------------------

We recommend you visit http://acegisecurity.org and read the "Suggested Steps"
page. This page is also included in the /docs directory of official release
ZIPs.

-------------------------------------------------------------------------------
DOCUMENTATION
-------------------------------------------------------------------------------

http://acegisecurity.org has a wide range of articles about Acegi Security,
including links to external resources. A copy of this web site is included in
the /docs directory of official release ZIPs.

Be sure to read the Reference Guide, which is available from the web site (and
/docs directory as described above). Every class also has thorough JavaDocs.
The core JavaDocs can be found in /docs/multiproject/acegi-security/apidocs/.

-------------------------------------------------------------------------------
OBTAINING SUPPORT
-------------------------------------------------------------------------------

If you need any help, please use the Acegi Security System for Spring forum
located at the Spring Community's forum site: 

  http://forum.springframework.org

If you start using Acegi Security in your project, please consider joining
the acegisecurity-developer mailing list. This is currently the best way to
keep informed about the project's status and provide feedback in design 
discussions. You can join at:

  https://lists.sourceforge.net/lists/listinfo/acegisecurity-developer.

Links to mailing list archives, the forums, and other useful resources are
available from http://acegisecurity.org.



$Id: readme.txt 2401 2007-12-24 15:56:53Z luke_t $
