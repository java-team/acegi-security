The acegi-security-doc team is pleased to announce the Acegi Security System 
for Spring 0.5 release! 

http://acegisecurity.org/

Acegi Security System for Spring 

Changes in this version include:

  New Features:

o Added single sign on support via Yale Central Authentication Service (CAS) 
o Added full support for HTTP Basic Authentication 
o Added caching for DaoAuthenticationProvider successful authentications 
o Added Burlap and Hessian remoting to Contacts sample application 
o Added pluggable password encoders including plaintext, SHA and MD5 
o Added pluggable salt sources to enhance security of hashed passwords 
o Added FilterToBeanProxy to obtain filters from Spring application context 
o Added support for prepending strings to roles created by JdbcDaoImpl 
o Added support for user definition of SQL statements used by JdbcDaoImpl 
o Added definable prefixes to avoid expectation of "ROLE_" GrantedAuthoritys 
o Added pluggable AuthenticationEntryPoints to SecurityEnforcementFilter 
o Added Apache Ant path syntax support to SecurityEnforcementFilter 
o Added filter to automate web channel requirements (eg HTTPS redirection) 

  Fixed bugs:

o Fixed FilterInvocation.getRequestUrl() to also include getPathInfo() 
o Fixed Contacts sample application tags 

  Changes:

o Updated JAR to Spring 1.0.1 
o Updated several classes to use absolute (not relative) redirection URLs 
o Refactored filters to use Spring application context lifecycle support 
o Improved constructor detection of nulls in User and other key objects 
o Established acegisecurity-developer mailing list 
o Documentation improvements  

Have fun!
-The acegi-security-doc team
      