The acegi-security-doc team is pleased to announce the Acegi Security System 
for Spring 0.3 release! 

http://acegisecurity.org/

Acegi Security System for Spring 

Changes in this version include:

  New Features:

o Added "in container" unit test system for container adapters and sample app 
o Added library extractor tool to reduce the "with deps" ZIP release sizes 
o Added unit test to the attributes sample 
o Added Jalopy source formatting 

  Changes:

o Modified all files to use net.sf.acegisecurity namespace 
o Renamed springsecurity.xml to acegisecurity.xml for consistency 
o Reduced length of ZIP and JAR filenames 
o Clarified licenses and sources for all included libraries 
o Updated documentation to reflect new file and package names 
o Setup Sourceforge.net project and added to CVS etc  

Have fun!
-The acegi-security-doc team
      