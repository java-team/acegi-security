#!/bin/sh -e

VERSION=$2
TAR=../acegi-security_$VERSION.orig.tar.gz
DIR=acegi-security-$VERSION
TAG=$(echo "acegi-security-parent-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export https://acegisecurity.svn.sourceforge.net/svnroot/acegisecurity/spring-security/tags/${TAG}/ $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
